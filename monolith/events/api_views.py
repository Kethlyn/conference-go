from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Conference, Location, State
from json import JSONEncoder
from datetime import datetime
from django.views.decorators.http import require_http_methods
import json
from .acls import use_pexels_to_get_pic, use_open_weather


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class ConferenceVODetailEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "ends",
        "starts",
        "created",
        "updated",
        "max_presentations",
        "max_attendees",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceVODetailEncoder,
        )
    else:
        content = json.loads(request.body)

    # Get the Location object and put it in the content dict
    try:
        location = Location.objects.get(id=content["location"])
        content["location"] = location
    except Location.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid location id"},
            status=400,
        )

    conference = Conference.objects.create(**content)
    return JsonResponse(
        conference,
        encoder=ConferenceDetailEncoder,
        safe=False,
    )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_conference(request, id):
    conference = Conference.objects.get(id=id)
    weather = use_open_weather(
        conference.location.city,
        conference.location.state.abbreviation,
    )
    return JsonResponse(
        {"conference": conference, "weather": weather},
        encoder=ConferenceDetailEncoder,
        safe=False,
    )
    # if request.method == "GET":
    #     conference = Conference.objects.get(id=id)
    #     return JsonResponse(
    #         conference, encoder=ConferenceDetailEncoder, safe=False
    #     )
    # elif request.method == "DELETE":
    #     count, _ = Conference.objects.filter(id=id).delete()
    #     return JsonResponse({"deleted": count > 0})

    # else:
    #     content = json.loads(request.body)
    #     try:
    #         location = Location.objects.get(id=content["location"])
    #         content["location"] = location
    #     except Location.DoesNotExist:
    #         return JsonResponse(
    #             {"message": "Invalid location id"},
    #             status=400,
    #         )
    #     Conference.objects.filter(id=id).update(**content)
    #     conference = Conference.objects.create(**content)
    #     return JsonResponse(
    #         conference,
    #         encoder=ConferenceDetailEncoder,
    #         safe=False,
    #     )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        # copied from create
        content = json.loads(request.body)
    try:
        # new code
        if "state" in content:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
    except State.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid state abbreviation"},
            status=400,
        )

    # new code
    Location.objects.filter(id=id).update(**content)

    # copied from get detail
    location = Location.objects.get(id=id)
    return JsonResponse(
        location,
        encoder=LocationDetailEncoder,
        safe=False,
    )


@require_http_methods(["GET", "POST"])
def api_list_location(request):
    if request.method == "GET":
        location = Location.objects.all()
        return JsonResponse(
            {"location": location},
            encoder=LocationListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state

        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        content["picture_url"] = use_pexels_to_get_pic(
            content["city"], content["state"].abbreviation
        )
        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
